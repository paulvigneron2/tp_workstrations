# Maîtrise de poste - Day 1

## Host OS


*Cette commande permettra de savoir le nom de la machine, l'OS, la version, l'architecture processeur (32-bit, 64-bit, ARM, etc) et laquantité RAM et modèle de la RAM etc...*
    
``PS C:\WINDOWS\system32> systeminfo``

        [...]
        Nom de l’hôte: DESKTOP-DM8M0FQ
        Nom du système d’exploitation: Microsoft Windows 10 Famille
        Version du système: 10.0.19041 N/A version 19041
        Type du système: x64-based PC
        Mémoire physique totale: 24 529 Mo
        [...]

---
## Devices
*Cette commande permettra de savoir la marque et le modèle de votre processeur(on pourra voir le nombre de coeur, processeur etc...).*


``PS C:\WINDOWS\system32> Get-CimInstance -ClassName Win32_Processor | Select-Object -ExcludeProperty "CIM*"``
    
    
    [...]
    DeviceID Name                                      Caption                            MaxClockSpeed SocketDesignation
    -------- ----                                      -------                              --------    ----- -----------------
    CPU0     Intel(R) Core(TM) i7-4720HQ CPU @ 2.60GHz Intel64 Family 6 Model 60 Stepping 3 2601          SOCKET 0
    [...]
---
*Cette commande permettra de connaître **la marque et le modèle de votre carte graphique**.*

``PS C:\WINDOWS\system32> wmic path win32_VideoController get name``

    Name
    NVIDIA GeForce GTX 970M
---
*Cette commande permettra **d'identifier la marque et le modèle de vos disques durs** et connaître la capacité de stockage*

``PS C:\WINDOWS\system32> Get-Disk``

    0      KINGSTON RBU-SNS8100S3128GD1  50026B72510DD2B3    Healthy    Online     119.24 GB GPT
    1      PNY CS900 960GB SSD     PNY441919110114033A2      Healthy    Online     894.25 GB MBR
  ---  
Cette commande permettra de **déterminer le système de fichier de chaque partition** et de voir les différentes partitions.
``PS C:\WINDOWS\system32> Get-Partition``

    PartitionNumber  DriveLetter Offset                                                                                                                    Size Type
    ---------------  ----------- ------                                                                                                                    ---- ----
    1                           1048576                                                                                                                 600 MB Recovery
    2                           630194176                                                                                                               300 MB System
    3                           944766976                                                                                                               128 MB Reserved
    4                C           1078984704                                                                                                           116.96 GB Basic
    5                           126662737920                                                                                                            859 MB Recovery


       DiskPath : \\?\scsi#disk&ven_pny&prod_cs900_960gb_ssd#4&17f163d2&0&030000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

    PartitionNumber  DriveLetter Offset                                                                                                                    Size Type
    ---------------  ----------- ------                                                                                                                    ---- ----
    1                E           1024                                                                                                                 894.25 GB IFS
    
---
## Users
Cette commande permettra de **voir la liste complète des utilisateurs  de la machine**.
``Get-LocalUser``
        
    Name               Enabled Description
    ----               ------- -----------
    Administrateur     False   Compte d’utilisateur d’administration
    DefaultAccount     False   Compte utilisateur géré par le système.
    Invité             False   Compte d’utilisateur invité
    Paul               True
    WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le     système pour les scénarios Windows Defender A...
---# Maîtrise de poste - Day 1

## Host OS


*Cette commande permettra de savoir le nom de la machine, l'OS, la version, l'architecture processeur (32-bit, 64-bit, ARM, etc) et laquantité RAM et modèle de la RAM etc...*
    
``PS C:\WINDOWS\system32> systeminfo``

        [...]
        Nom de l’hôte: DESKTOP-DM8M0FQ
        Nom du système d’exploitation: Microsoft Windows 10 Famille
        Version du système: 10.0.19041 N/A version 19041
        Type du système: x64-based PC
        Mémoire physique totale: 24 529 Mo
        [...]

---
## Devices
*Cette commande permettra de savoir la marque et le modèle de votre processeur(on pourra voir le nombre de coeur, processeur etc...).*


``PS C:\WINDOWS\system32> Get-CimInstance -ClassName Win32_Processor | Select-Object -ExcludeProperty "CIM*"``
    
    
    [...]
    DeviceID Name                                      Caption                            MaxClockSpeed SocketDesignation
    -------- ----                                      -------                              --------    ----- -----------------
    CPU0     Intel(R) Core(TM) i7-4720HQ CPU @ 2.60GHz Intel64 Family 6 Model 60 Stepping 3 2601          SOCKET 0
    [...]
---
*Cette commande permettra de connaître **la marque et le modèle de votre carte graphique**.*

``PS C:\WINDOWS\system32> wmic path win32_VideoController get name``

    Name
    NVIDIA GeForce GTX 970M
---
*Cette commande permettra **d'identifier la marque et le modèle de vos disques durs** et connaître la capacité de stockage*

``PS C:\WINDOWS\system32> Get-Disk``

    0      KINGSTON RBU-SNS8100S3128GD1  50026B72510DD2B3    Healthy    Online     119.24 GB GPT
    1      PNY CS900 960GB SSD     PNY441919110114033A2      Healthy    Online     894.25 GB MBR
  ---  
Cette commande permettra de **déterminer le système de fichier de chaque partition** et de voir les différentes partitions.
``PS C:\WINDOWS\system32> Get-Partition``

    PartitionNumber  DriveLetter Offset                                                                                                                    Size Type
    ---------------  ----------- ------                                                                                                                    ---- ----
    1                           1048576                                                                                                                 600 MB Recovery
    2                           630194176                                                                                                               300 MB System
    3                           944766976                                                                                                               128 MB Reserved
    4                C           1078984704                                                                                                           116.96 GB Basic
    5                           126662737920                                                                                                            859 MB Recovery


       DiskPath : \\?\scsi#disk&ven_pny&prod_cs900_960gb_ssd#4&17f163d2&0&030000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

    PartitionNumber  DriveLetter Offset                                                                                                                    Size Type
    ---------------  ----------- ------                                                                                                                    ---- ----
    1                E           1024                                                                                                                 894.25 GB IFS
    
---

Cette commande permettra de **voir la liste complète des utilisateurs  de la machine**.
``Get-LocalUser``
        
    Name               Enabled Description
    ----               ------- -----------
    Administrateur     False   Compte d’utilisateur d’administration
    DefaultAccount     False   Compte utilisateur géré par le système.
    Invité             False   Compte d’utilisateur invité
    Paul               True
    WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le     système pour les scénarios Windows Defender A...
---

#III. Gestion de softs
    Un paquet est une archive comprenant les fichiers informatiques, les informations et procédures nécessaires à l'installation d'un logiciel sur un système d'exploitation au sein d'un agrégat logiciel, en s'assurant de la cohérence fonctionnelle du système ainsi modifié.

Le gestionnaire de paquets permet d'effectuer différentes opérations sur les paquets disponibles :

Installation, mise à jour, et désinstallation plus rapide
Vérification des sommes de contrôle de chaque paquet récupéré pour en vérifier l'intégrité 
Vérification des dépendances logicielles afin d'obtenir une version fonctionnelle d'un paquetage
La sécurité global avec l'utilisateur est importante. Pendant un téléchargement le site officiel et l'utilisateur doit seulement être en contact entre eux personne d'autre dois être avec eux.
